/*******************************************************************************
 * Copyright 2024  Sphynx Technology Solutions
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package ch.sphynx.receiver;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class ReceiveMessage {

  @Value("${spring.rabbitmq.exchange-name}")
  private static String exchangeName ;

  @Value("${spring.rabbitmq.host}")
  private static String rabbitHost;

  @Value("${spring.rabbitmq.username}")
  private static String rabbitUsername;

  @Value("${spring.rabbitmq.password}")
  private static String rabbitPassword;

  @Value("${spring.rabbitmq.virtual-host}")
  private static String rabbitVirtualHost;

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(rabbitHost);
    factory.setUsername(rabbitUsername);
    factory.setPassword(rabbitPassword);
    factory.setVirtualHost(rabbitVirtualHost);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(exchangeName, "topic");
    String queueName = channel.queueDeclare().getQueue();

    if (argv.length < 1) {
      log.error("Usage: receiver.ReceiveLogsTopic [binding_key]...");
      System.exit(1);
    }

    for (String bindingKey : argv) {
      channel.queueBind(queueName, exchangeName, bindingKey);
    }

   log.info(" [*] Waiting for messages. To exit press CTRL+C");

    DeliverCallback deliverCallback =
        (consumerTag, delivery) -> {
          String message = new String(delivery.getBody(), "UTF-8");
         log.info(
              " [x] Received '" + delivery.getEnvelope().getRoutingKey() + "':'" + message + "'");
        };
    channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
  }
}
